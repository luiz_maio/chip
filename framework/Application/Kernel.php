<?php

declare(strict_types=1);

namespace Chip\Application;

final class Kernel
{
    public function __construct(Response $response)
    {
        $response->dispatch();
    }
}
