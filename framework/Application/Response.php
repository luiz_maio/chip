<?php

declare(strict_types=1);

namespace Chip\Application;

use Exception;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;

use Symfony\Component\HttpKernel;

final class Response
{
    /**
     * Symfony response.
     *
     * @var Symfony\Component\HttpFoundation\Response
     */
    private $response;

    public function __construct(array $routes)
    {
        $routeCollection  = new RouteCollection();
        $request          = Request::createFromGlobals();

        try {
            foreach ($routes as $name => $configurations) {
                $route = new Route(
                    $configurations['path'],
                    ['_controller' => $configurations['controller'] . '::' . $configurations['action']],
                    $configurations['validations'] ?? []
                );

                // Add Route object(s) to RouteCollection object
                $routeCollection->add($name, $route);
            }

            $controllerResolver = new HttpKernel\Controller\ControllerResolver();
            $argumentResolver   = new HttpKernel\Controller\ArgumentResolver();

            $context = new RequestContext();
            $context->fromRequest($request);
            $matcher = new UrlMatcher($routeCollection, $context);

            $request->attributes->add($matcher->match(
                $request->getPathInfo()
            ));

            $controller = $controllerResolver->getController($request);
            $arguments  = $argumentResolver->getArguments($request, $controller);

            $this->response = call_user_func_array($controller, $arguments);
        } catch (ResourceNotFoundException $exception) {
            $this->response = new SymfonyResponse('URI path not found.', 404);
        } catch (Exception $exception) {
            $this->response = new SymfonyResponse('An error occurred', 500);
        }
    }

    public function dispatch() : SymfonyResponse
    {
        return $this->response->send();
    }
}
