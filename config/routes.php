<?php

use App\Controller\IndexController;
use App\Controller\TestController;

return [
    'index' => [
        'path'        => '/',
        'controller'  => IndexController::class,
        'action'      => 'helloWorld'
    ],
    'test.index' => [
        'path'        => '/test',
        'controller'  => TestController::class,
        'action'      => 'index'
    ],
    'test.show' => [
        'path'        => '/test/{id}',
        'controller'  => TestController::class,
        'action'      => 'show',
        'validations' => [
            'id' => '\d+',
        ]
    ]
];