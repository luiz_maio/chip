<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

final class TestController
{
    public function index()
    {
        return new Response('Hi!');
    }

    public function show($id)
    {
        return new Response('ID: ' . $id);
    }
}
