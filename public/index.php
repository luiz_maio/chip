<?php

use Chip\Application\Response;

require_once __DIR__ . '/../vendor/autoload.php';

$routes = require_once __DIR__ . '/../config/routes.php';

// Dispatch application
new Chip\Application\Kernel(
    new Response($routes)
);
